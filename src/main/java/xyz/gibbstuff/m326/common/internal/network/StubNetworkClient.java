/*§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
 § The MIT License (MIT)
 § Copyright (c) 2016 Joel Messerli
 §
 § Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 § and associated documentation files (the "Software"), to deal in the Software without restriction,
 § including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 § and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 § to do so, subject to the following conditions:
 §
 § The above copyright notice and this permission notice shall be included in all copies or
 § substantial portions of the Software.
 §
 § THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 § INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 § PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 § COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 § AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 § WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§*/

package xyz.gibbstuff.m326.common.internal.network;

import xyz.gibbstuff.m326.common.interfaces.network.MessageConsumer;
import xyz.gibbstuff.m326.common.interfaces.network.NetworkClient;
import xyz.gibbstuff.m326.common.interfaces.protocol.Message;

/**
 * A simple stub {@link NetworkClient}. Ignores every method call. Well suited for development if
 * the final {@link NetworkClient} is not finished yet
 */
public class StubNetworkClient implements NetworkClient {
    public void addMessageConsumer(MessageConsumer consumer) {
    }

    public void removeMessageConsumer(MessageConsumer consumer) {
    }

    public void send(Message message) {
    }
}
